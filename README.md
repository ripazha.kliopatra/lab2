# lab2



## Цель

1. Развить практические навыки использования современного стека инструментов сбора и аналитической обработки информации о сетвом трафике
2. Освоить базовые подходы блокировки нежелательного сетевого трафика
3. Закрепить знания о современных сетевых протоколах прикладного уровня

## Исходные данные

1. Средство виртуализации VirtualBox
2. Kali linux
3. Утилиты Wireshark и Zeek

## Задание

1. Собрать сетевой трафик (объемом не менее 100 Мб)
2. Выделить метаинформацию сетевого трафика с помощью утилиты Zeek
3. Собрать данные об источниках нежелательного трафика, например – https://github.com/StevenBlack
4. Написать программу на любом удобном языке (python, bash, R …), которая подсчитывает процент нежелательного трафика в собранном на этапе 1.
5. Оформить отчет в соответствии с шаблоном

## Общий план выполнения

1. Собрать трафик с помощью утилиты wireshark
2. Отсортировать трафик с помощью утилиты wireshark
3. Написать программу на языке программирования Python 3.9

# Содержание ЛР

## Шаг 1. Собрать сетевой трафик (объемом не менее 100 Мб)
С помощью утилиты Wireshark соберем более 100мб трафика. Для захвата нежелательного трафика используя ссылки из файла hosts с https://github.com/StevenBlack
![](1.JPG)
Сохраним файл с расширением .pcapng
## Шаг 2. Выделение метаинформации сетевого трафика с помощью утилиты Zeek
Zeek раскидывает метаинформацию о трафике в соответствующие файлы по протоколам. 
Передаем полученный ранее трафик в Zeek: `sudo zeek -Cr home/kali/lab2/1.pcapng`

![](2.JPG)

Нас будет интересовать прежде всего файл DNS запросов. Он же: dns.log
## Шаг 3. Собрать данные об источниках нежелательного трафика
Получаем данные об источниках нежелательного трафика, с ресурса – https://github.com/StevenBlack. Храниться они будут в файле hosts

## Шаг 4. Написать программу, котрая подсчитывает процент нежелательного трафика
Напишем скрипт для подсчёта процента нежелательного трафика: 
```
with open('hosts', 'r') as dictFile:
    dict = dictFile.readlines()
    dict = [x[:-1] for x in dict]

with open('dns.log', 'r') as recordsFile:
    records = recordsFile.readlines()
    records = [x.split('\x09')[9] for x in records if not x.startswith('#')]
    
intersection = [x for x in records if x in dict]
print(len(intersection) / len(records) * 100)
```
Код можно запустить непосредственно в терминале kali linux


# Оценка результата
Процент нежелательного трафика: ~43.5%

![](3.JPG)

Такой высокий процент нежелательного трафика получен из-за использования заведомо нежелательных ссылок из hosts.txt
# Вывод
В рамках выполнения данной работы были развиты практические навыки использования современного стека инструментов сбора и аналитической обработки информации о сетевом трафике; освоены базовые подходы блокировки нежелательного сетевого трафика; закреплены знания о современных сетевых протоколах прикладного уровня.


